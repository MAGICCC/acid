package net.rizon.acid.core;

import net.rizon.acid.plugins.Plugin;
import java.util.List;
import net.rizon.acid.conf.Client;

public class AcidUser extends User
{
	public Plugin pkg;
	public Client client;

	public AcidUser(final String nick, final String user, final String host, final String vhost, final String name, final String modes)
	{
		super(nick, user, host, vhost, name, AcidCore.me, AcidCore.getTS(), AcidCore.getTS(), modes, User.generateUID(), "255.255.255.255");
	}

	public AcidUser(Plugin pkg, Client c)
	{
		super(c.nick, c.user, c.host, c.vhost, c.name, AcidCore.me, AcidCore.getTS(), AcidCore.getTS(), c.modes, User.generateUID(), "255.255.255.255");
		this.pkg = pkg;
		this.client = c;

		if (client != null && client.channels != null)
			for (String ch : client.channels)
				this.joinChan(Acidictive.conf.getChannelNamed(ch));
	}

	public void introduce()
	{
		Protocol.uid(this);

		if (this.getNSPass() != null && !this.getNSPass().isEmpty())
			Protocol.privmsg(this.getUID(), "NickServ@services.rizon.net", "IDENTIFY " + this.getNSPass()); // XXX

		for (Channel chan : this.getChannels())
			Protocol.join(this, chan);
	}

	public void joinChan(final String channel)
	{
		Channel chan = Channel.findChannel(channel);
		if (chan != null && this.isOnChan(chan))
			return;

		if (chan == null)
		{
			chan = new Channel(channel, AcidCore.getTS());
			chan.setMode('n'); chan.setMode('t');
		}
		if (!AcidCore.me.isBursting())
			Protocol.join(this, chan);
		chan.addUser(this, "");
		this.addChan(chan);
	}

	public void partChan(Channel chan)
	{
		if (!this.isOnChan(chan))
			return;

		Protocol.part(this, chan.getName());
		chan.removeUser(this);
		this.remChan(chan);
	}

	public void quit(final String reason)
	{
		Protocol.quit(this, reason);
		this.onQuit();
	}

	public final String getNSPass()
	{
		if (client != null)
			return client.nspass;
		else
			return null;
	}

	public net.rizon.acid.conf.Command findConfCommand(String name, String channel)
	{
		if (client != null && this.client.commands != null)
			for (net.rizon.acid.conf.Command c : this.client.commands)
				if (c.name.equalsIgnoreCase(name) && (channel == null || c.allowsChannel(channel)))
					return c;
		return null;
	}

	public List<net.rizon.acid.conf.Command> getConfCommands()
	{
		return this.client.commands;
	}
}

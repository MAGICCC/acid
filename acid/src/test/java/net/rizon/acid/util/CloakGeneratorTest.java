/*
 * Copyright (c) 2017, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.util;

import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author Orillion {@literal <orillion@rizon.net>}
 */
@RunWith(Parameterized.class)
public class CloakGeneratorTest
{
	private static final String KEY_ONE = "cloakKey1";
	private static final String KEY_TWO = "cloakKey2";
	private static final String KEY_THREE = "cloakKey3";
	private static final String NETWORK = "Rizon-";

	private static final String KEYS[] =
	{
		KEY_ONE, KEY_TWO, KEY_THREE
	};

	private final String host;
	private final String expected;

	private CloakGenerator cloakGenerator;

	public CloakGeneratorTest(String host, String expected)
	{
		this.host = host;
		this.expected = expected;
	}

	@Before
	public void setUp()
	{
		this.cloakGenerator = new CloakGenerator(KEYS, NETWORK);
	}

	@After
	public void tearDown()
	{
		this.cloakGenerator = null;
	}

	@Parameterized.Parameters
	public static Collection cloaks()
	{
		return Arrays.asList(new Object[][]
		{
			{
				"cakes.net", NETWORK + "77A67854.net"
			},
			{
				"cakes.10.12.net", NETWORK + "F0DDF00E.net"
			},
			{
				"ip42.cakes.10.net", NETWORK + "7D504780.cakes.10.net"
			},
			{
				"1.2.3.4.5.6.7.9.super.google.irc", NETWORK + "DF13E3E9.super.google.irc"
			},
			// IPv4
			{
				"127.0.0.1", "3317843C.232EC1D0.89C2F6D2.IP"
			},
			{
				"127.0.1.1", "4FD42D4.52356747.89C2F6D2.IP"
			},
			{
				"128.0.0.0", "EFB21FC1.CB8E5CE9.45DD05EE.IP"
			},
			{
				"0.0.0.0", "5A50FD90.833E8F76.36201F95.IP"
			},
			{
				"255.255.255.255", "C7C7D066.A18B600F.67F6A496.IP"
			},
			{
				"1.1.1.1", "9F5EF322.5C907051.DA359507.IP"
			},
			{
				"1.2.3.4", "F3F65368.657FBCBB.9E35423B.IP"
			},
			{
				"0.255.0.255", "5CE3D1E6.C5C8CABB.CFF89735.IP"
			},
			// IPv6
			{
				"0:0:0:0:0:0:0:0", "A546741B:7AAF89D7:6983D238:IP"
			},
			{
				"ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", "923A446F:6E326E59:72EEFF3:IP"
			},
			{
				"abcd:1234:12:34:45AB:CCDD:10:0000", "E46E5F07:A8DA7EAA:E8AD9905:IP"
			}
		});
	}

	@Test
	public void testCloak()
	{
		String result = this.cloakGenerator.cloak(this.host);
		Assert.assertEquals(this.expected, result);
	}
}

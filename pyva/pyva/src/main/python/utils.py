import re
import time
from datetime import datetime
from htmlentitydefs import name2codepoint

def unix_time(datetime):
	return int(time.mktime(datetime.timetuple()))

def parse_timespan(text):
	buffer = ''
	duration = 0

	for char in text:
		if char == 'd':
			duration += int(buffer) * 24 * 60 * 60
			buffer = ''
		elif char == 'h':
			duration += int(buffer) * 60 * 60
			buffer = ''
		elif char == 'm':
			duration += int(buffer) * 60
			buffer = ''
		elif char == 's':
			duration += int(buffer)
			buffer = ''
		else:
			buffer += char

	return duration

def get_timespan(date):
	td = datetime.now() - date
	if td.days > 365:
		y = int(td.days/365)
		return '%d year%s' % (y, 's' if y > 1 else '')
	elif td.days > 0:
		d = td.days
		return '%d day%s' % (d, 's' if d > 1 else '')
	elif td.seconds > 3600:
		h = int(td.seconds/3600)
		return '%d hour%s' % (h, 's' if h > 1 else '')
	elif td.seconds > 60:
		m = int(td.seconds/60)
		return '%d minute%s' % (m, 's' if m > 1 else '')
	else:
		s = td.seconds
		return '%d second%s' % (s, 's' if s > 1 else '')

def format_name(name):
	if name.endswith('s'):
		return "%s'" % name
	else:
		return "%s's" % name

def format_thousand(number, add_prefix = False):
	if not isinstance(number, int):
		return str(number)

	text = str(abs(number))

	length = len(text)
	count = int((length - 1) / 3)

	for i in xrange(1, count + 1):
		text = text[:length - (i * 3)] + ',' + text[length - (i * 3):]

	if number < 0:
		text = '-' + text
	elif add_prefix:
		text = '+' + text

	return text

def format_hms(seconds):
	hours = seconds / 3600
	seconds -= 3600*hours
	minutes = seconds / 60
	seconds -= 60*minutes
	if hours == 0:
		return "%02d:%02d" % (minutes, seconds)
	return "%02d:%02d:%02d" % (hours, minutes, seconds)

def format_ascii_irc(message):
	return message.replace('@errsep', '@b@c4::@o').replace('@nsep', '@b@c7::@o').replace('@sep', '@b@c3::@o').replace('@b', chr(2)).replace('@c', chr(3)).replace('@o', chr(15)).replace('@u', chr(31))

def strip_ascii_irc(message):
	stripped = ''

	for char in message:
		if char not in [chr(2), chr(15), chr(22), chr(31)]: #Not actually stripping color codes, but we don't need that (yet)
			stripped += char

	return stripped

def unescape(s):
	"unescape HTML code refs; c.f. http://wiki.python.org/moin/EscapingHtml"

	# XXX: There must be a better way than hardcoding these
	name2codepoint['#39'] = 39
	name2codepoint['#215'] = 215
	name2codepoint['#8260'] = 8260

	return re.sub('&(%s);' % '|'.join(name2codepoint),
		lambda m: unichr(name2codepoint[m.group(1)]), s)

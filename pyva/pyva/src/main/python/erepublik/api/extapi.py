from urllib import quote
from feed import get_json

def get_rankings(arg):
	if isinstance(arg, int):
		url = 'http://api.1way.it/erep/rankings/id/%d' % arg
	else:
		url = 'http://api.1way.it/erep/rankings/name/' + quote(arg.encode('UTF-8'))
	
	return get_json(url)

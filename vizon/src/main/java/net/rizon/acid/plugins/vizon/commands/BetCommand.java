/*
 * Copyright (c) 2016, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon.commands;

import java.util.Optional;
import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Command;
import net.rizon.acid.core.User;
import net.rizon.acid.plugins.vizon.Bet;
import net.rizon.acid.plugins.vizon.BetValidator;
import net.rizon.acid.plugins.vizon.Vizon;
import net.rizon.acid.plugins.vizon.VizonBet;
import net.rizon.acid.plugins.vizon.db.VizonDrawing;
import net.rizon.acid.plugins.vizon.db.VizonUser;

/**
 * This class handles the user's Bet command.
 *
 * @author orillion <orillion@rizon.net>
 */
public class BetCommand extends Command
{
	public BetCommand()
	{
		super(1, 1);
	}

	@Override
	public void Run(User source, AcidUser to, Channel c, String[] args)
	{
		// @TODO: Perhaps we should move all replies to a config setting?
		if (!source.isIdentified())
		{
			// User has not identified to NickServ.
			Acidictive.reply(source, to, c, "You need to identify before you can participate");
			return;
		}

		if (!source.isOnChan(Vizon.getVizonChannel()))
		{
			// User is not on the VIzon channel.
			Acidictive.reply(source, to, c, String.format("You need to be on %s before placing a bet", Vizon.getConf().vizonChannel));
			return;
		}

		VizonDrawing drawing = Vizon.getVizonDatabase().getNextDrawing();

		if (drawing == null)
		{
			// @TODO: Tell user when bet will open.
			Acidictive.reply(source, to, c,
					"Betting is not open yet");
			return;
		}

		switch (drawing.getState())
		{
			case PREPARING:
				Acidictive.reply(source, to, c,
						"Betting is not open yet");
				return;
			case CLOSED:
				Acidictive.reply(source, to, c,
						"Betting is closed, drawing will begin shortly");
				return;
			case OPEN:
			default:
				break;
		}

		// Find or create the user.
		VizonUser user = Vizon.getVizonDatabase().findOrCreateUser(source.getNick());

		if (user == null)
		{
			Acidictive.reply(source, to, c,
					"Unable to find or create a user object");
			return;
		}

		VizonBet vizonBet = Vizon.getVizonDatabase().findBetForUserAndDrawing(user, drawing);

		if (vizonBet != null)
		{
			Acidictive.reply(source, to, c,
					String.format("You have already placed a bet, your bet is %d %d %d %d %d %d",
							vizonBet.getBet().getFirst(),
							vizonBet.getBet().getSecond(),
							vizonBet.getBet().getThird(),
							vizonBet.getBet().getFourth(),
							vizonBet.getBet().getFifth(),
							vizonBet.getBet().getSixth()));
			return;
		}

		Optional<Bet> betOpt = BetValidator.validate(args[0]);

		if (!betOpt.isPresent())
		{
			// User made an invalid bet.
			// @TODO: Move reply to config setting.
			Acidictive.reply(source, to, c,
					"Invalid bet. Please make sure you pick 6 non-repeating numbers between 1 and 29. Read http://s.rizon.net/VIzon for details.");
			return;
		}

		Bet bet = betOpt.get();

		boolean success = Vizon.getVizonDatabase().createBetForUser(user, drawing, bet);

		if (success)
		{
			// Notify user bet was stored.
			Acidictive.reply(
					source,
					to,
					c,
					String.format("Bet placed: %d %d %d %d %d %d",
							bet.getFirst(),
							bet.getSecond(),
							bet.getThird(),
							bet.getFourth(),
							bet.getFifth(),
							bet.getSixth()));
		}
		else
		{
			// Notify user that we failed to save the bet in the database.
			Acidictive.reply(source, to, c,
					"Failed to store bet, if this problem persists, please contact a staff member");
		}
	}
}

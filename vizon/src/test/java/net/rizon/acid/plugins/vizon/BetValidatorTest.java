/*
 * Copyright (c) 2016, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon;

import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;

/**
 * Unit tests for the bet validator.
 *
 * @author orillion <orillion@rizon.net>
 */
public class BetValidatorTest
{
	/**
	 * Test whether or not the validate method returns an empty optional when
	 * you present null to it.
	 */
	@Test
	public void validateNullTest()
	{
		String bet = null;

		Optional<Bet> result = BetValidator.validate(bet);

		Assert.assertFalse(result.isPresent());
	}

	/**
	 * Test whether or not the validate method returns an empty optional when
	 * you present an empty to it.
	 */
	@Test
	public void validateEmptyStringTest()
	{
		String bet = "";

		Optional<Bet> result = BetValidator.validate(bet);

		Assert.assertFalse(result.isPresent());
	}

	/**
	 * Test whether or not the validate method returns an empty optional when
	 * you present an incorrect string to it.
	 */
	@Test
	public void validateNotEnoughNumbersTest()
	{
		String bet = "1 2 3";

		Optional<Bet> result = BetValidator.validate(bet);

		Assert.assertFalse(result.isPresent());
	}

	/**
	 * Test whether or not the validate method returns an empty optional when
	 * you present an incorrect string to it.
	 */
	@Test
	public void validateTooManyNumbersTest()
	{
		String bet = "1 2 3 4 5 6 7";

		Optional<Bet> result = BetValidator.validate(bet);

		Assert.assertFalse(result.isPresent());
	}

	/**
	 * Test whether or not the validate method returns an empty optional when
	 * you present an incorrect string to it.
	 */
	@Test
	public void validateDoubleNumbersTest()
	{
		String bet = "1 2 1 3 4 5";

		Optional<Bet> result = BetValidator.validate(bet);

		Assert.assertFalse(result.isPresent());
	}

	/**
	 * Test whether or not the validate method returns an empty optional when
	 * you present an incorrect string to it.
	 */
	@Test
	public void validateNegativeNumbersTest()
	{
		String bet = "1 2 -11 3 4 5";

		Optional<Bet> result = BetValidator.validate(bet);

		Assert.assertFalse(result.isPresent());
	}

	/**
	 * Test whether or not the validate method returns an empty optional when
	 * you present an incorrect string to it.
	 */
	@Test
	public void validateOutLowerBoundTest()
	{
		String bet = "1 2 0 3 4 5";

		Optional<Bet> result = BetValidator.validate(bet);

		Assert.assertFalse(result.isPresent());
	}

	/**
	 * Test whether or not the validate method returns an empty optional when
	 * you present an incorrect string to it.
	 */
	@Test
	public void validateOutUpperBoundTest()
	{
		String bet = "1 2 30 3 4 5";

		Optional<Bet> result = BetValidator.validate(bet);

		Assert.assertFalse(result.isPresent());
	}

	/**
	 * Test whether or not the validate method returns an filled optional when
	 * you present a correct string to it.
	 */
	@Test
	public void validateOnLowerBoundTest()
	{
		String bet = "1 2 3 4 5 6";

		Optional<Bet> result = BetValidator.validate(bet);

		Assert.assertTrue(result.isPresent());
		Assert.assertEquals(1, result.get().getFirst());
		Assert.assertEquals(2, result.get().getSecond());
		Assert.assertEquals(3, result.get().getThird());
		Assert.assertEquals(4, result.get().getFourth());
		Assert.assertEquals(5, result.get().getFifth());
		Assert.assertEquals(6, result.get().getSixth());
	}

	/**
	 * Test whether or not the validate method returns an filled optional when
	 * you present a correct string to it.
	 */
	@Test
	public void validateOnUpperBoundTest()
	{
		String bet = "1 2 3 4 5 29";

		Optional<Bet> result = BetValidator.validate(bet);

		Assert.assertTrue(result.isPresent());
		Assert.assertEquals(1, result.get().getFirst());
		Assert.assertEquals(2, result.get().getSecond());
		Assert.assertEquals(3, result.get().getThird());
		Assert.assertEquals(4, result.get().getFourth());
		Assert.assertEquals(5, result.get().getFifth());
		Assert.assertEquals(29, result.get().getSixth());
	}

	/**
	 * Test whether or not the validate method returns an filled optional when
	 * you present a correct string to it.
	 */
	@Test
	public void validateResultRemainsUnsortedTest()
	{
		String bet = "9 2 11 4 21 29";

		Optional<Bet> result = BetValidator.validate(bet);

		Assert.assertTrue(result.isPresent());
		Assert.assertEquals(9, result.get().getFirst());
		Assert.assertEquals(2, result.get().getSecond());
		Assert.assertEquals(11, result.get().getThird());
		Assert.assertEquals(4, result.get().getFourth());
		Assert.assertEquals(21, result.get().getFifth());
		Assert.assertEquals(29, result.get().getSixth());
	}

	/**
	 * Test whether or not the validator does not mind multiple spaces in front.
	 * <p>
	 * Reasoning: Users can accidentally type a space and it's something that
	 * can be solved easy with a removeEmptyEntries call in standard java
	 * library.
	 */
	@Test
	public void validateAccidentalSpaceFrontTest()
	{
		String bet = "   9 2 11 4 21 29";

		Optional<Bet> result = BetValidator.validate(bet);

		Assert.assertTrue(result.isPresent());
		Assert.assertEquals(9, result.get().getFirst());
		Assert.assertEquals(2, result.get().getSecond());
		Assert.assertEquals(11, result.get().getThird());
		Assert.assertEquals(4, result.get().getFourth());
		Assert.assertEquals(21, result.get().getFifth());
		Assert.assertEquals(29, result.get().getSixth());
	}

	/**
	 * Test whether or not the validator does not mind multiple spaces at the
	 * end.
	 * <p>
	 * Reasoning: Users can accidentally type a space and it's something that
	 * can be solved easy with a removeEmptyEntries call in standard java
	 * library.
	 */
	@Test
	public void validateAccidentalSpaceEndTest()
	{
		String bet = "9 2 11 4 21 29   ";

		Optional<Bet> result = BetValidator.validate(bet);

		Assert.assertTrue(result.isPresent());
		Assert.assertEquals(9, result.get().getFirst());
		Assert.assertEquals(2, result.get().getSecond());
		Assert.assertEquals(11, result.get().getThird());
		Assert.assertEquals(4, result.get().getFourth());
		Assert.assertEquals(21, result.get().getFifth());
		Assert.assertEquals(29, result.get().getSixth());
	}

	/**
	 * Test whether or not the validator does not mind multiple spaces in
	 * between.
	 * <p>
	 * Reasoning: Users can accidentally type a space and it's something that
	 * can be solved easy with a removeEmptyEntries call in standard java
	 * library.
	 */
	@Test
	public void validateAccidentalSpaceBetweenTest()
	{
		String bet = "9   2 11 4    21  29";

		Optional<Bet> result = BetValidator.validate(bet);

		Assert.assertTrue(result.isPresent());
		Assert.assertEquals(9, result.get().getFirst());
		Assert.assertEquals(2, result.get().getSecond());
		Assert.assertEquals(11, result.get().getThird());
		Assert.assertEquals(4, result.get().getFourth());
		Assert.assertEquals(21, result.get().getFifth());
		Assert.assertEquals(29, result.get().getSixth());
	}

	/**
	 * Test whether or not the validator returns an empty Optional when
	 * presented with gibberish.
	 */
	@Test
	public void validateNoNumbersTest()
	{
		String bet = "hello world!";

		Optional<Bet> result = BetValidator.validate(bet);

		Assert.assertFalse(result.isPresent());
	}

	/**
	 * Test whether or not the validator returns an empty Optional when
	 * presented with gibberish.
	 */
	@Test
	public void validateTextAfterNumbersTest()
	{
		String bet = "1 2 3 4 5 6 hello world!";

		Optional<Bet> result = BetValidator.validate(bet);

		Assert.assertFalse(result.isPresent());
	}
}
